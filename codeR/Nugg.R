source("BaseFunctions.R")
source("algo4Base.R")
source("DataVisu.R")


n=1000 ## Test set size
p=8  ## Input number
nsim=25 ## simulation number
nproc=25 ## number of cores used for the simulation
ncores=1 ### proc numbers for the algorithm

### Parameters for the GP simulation
coef.cov =  c(0.5,0.9,1.8) ## theta
groupe=list(1,2:3,4:8) ## groups
grrep=sapply(groupe,length) ## number of variable in each groupes
gcov=gcovProd ## covariance structure

xt = sobol(n, p)
xt=data.frame(xt)
names(xt)=paste("x",1:ncol(xt),paste="",sep="")

navec=c(p*10,p*20,p*50,p*100) ### Learning set size
navec=400
nug=c(0,0.02,0.05,0.1,0.3)  ### Nugget


Nugg=list()
Nugg$Parametre=data.table()
Nugg$Qualite=data.table()
save(Nugg,file="Nuggtemp.Rdata")

sauvegarde=list()
length(sauvegarde)=length(navec)+length(nug)
for ( j in 1:length(navec))
{
  na=navec[j]
  xa= maximinSA_LHS(lhsDesign(na, p,seed=42)$design)$design
  xa=data.frame(xa)
  names(xa)=names(xt)
  for ( k in 1:length(nug))
  {
    sink("itNugg.txt")
    print(navec[j])
    print(nug[k])
    sink()
    res=list()
    length(res)=nsim
    res=mclapply(res,function(al)
    {
      x=rbind(xa,xt)
      x=data.frame(x)
      names(x)=paste("x",1:ncol(x),paste="",sep="")
      covtype= "matern5_2"
      coef.cov2=rep(coef.cov,grrep)
      
      sigma =1
      trend =1
      nugget=nug[k]
      formula = ~1    # quadratic trend (beware to the usual I operator)
      
      model = km(formula, design=data.frame(x=x), response=rep(0,n+na), 
                 covtype=covtype, coef.trend=trend, coef.cov=coef.cov2, 
                 coef.var=sigma^2, nugget=nugget)
      y = simulate(model, nsim=1, newdata=NULL)
      y=y[1,]
      ya=y[1:na]
      ytest=y[(na+1):(n+na)]
      
      res4Iso=alg4(xa,ya,BICloglik,gcovIso,ncores=ncores)
      res4Prod=alg4(xa,ya,BICloglik,gcovProd,ncores=ncores)
      taniso=system.time({aniso=km(formula=~1, xa, ya, covtype="matern5_2",upper=rep(10,ncol(xa)),nugget.estim=FALSE)})
      res5=list(best=as.list(1:p),critere=(-2*logLik(aniso) + log(aniso@n)*p),model=aniso,time=taniso,stock="No data storage")
      taniso=system.time({aniso=km(formula=~1, xa, ya, covtype="matern5_2",iso=TRUE,upper=10)})
      res6=list(best=list(1:p),critere=(-2*logLik(aniso) + log(aniso@n)*p),model=aniso,time=taniso,stock="No data storage")
      groupeiso=list(1:p)
      coviso=gcov(groupeiso)
      tisopg=system.time({isopg=models(coviso,xa,ya)})
      if ( class(isopg)!="gp")
      {
        res7=list(best=list(1:p),critere=NULL,model=isopg,time=tisopg,stock="No data storage")
      }
      if ( class(isopg)=="gp")
      {
        res7=list(best=list(1:p),critere=BICloglik(isopg),model=isopg,time=tisopg,stock="No data storage")
      }
      al=list(alg4Iso=res4Iso,alg4Prod=res4Prod,Aniso=res5,IsoProd=res6,Iso=res7,ytest=ytest,xt=xt)
      return(al)
    },mc.cores = min(nproc,detectCores()-2))
    sauvegarde[[k+j]]=res
    save(sauvegarde,file="Nuggres.Rdata")
    Part=PQres(res,nproc)
    Parametre=Part$Parametre
    Qualite=Part$Qualite
    load(file="Nuggtemp.Rdata")
    Nugg$Parametre=rbind(Nugg$Parametre,data.table(taille=rep(navec[j],nrow(Parametre)),nugget=rep(nug[k],nrow(Parametre)),Parametre))
    Nugg$Qualite  =rbind(Nugg$Qualite  ,data.table(taille=rep(navec[j],nrow(Qualite)),nugget=rep(nug[k],nrow(Qualite  )),Qualite  ))
    save(Nugg,file="Nuggtemp.Rdata")
  }
}


save(Nugg,file="Nugg.Rdata")

### data visualization 

### prediction quality comprison alg4 Aniso and Iso
nugg=Nugg$Qualite$nugget
size=400

repet=NULL
for(i in 1:length(unique(nugg)))
{
  repet=c(repet,rep(((i-1)*3+1):(3*i),nsim))
}


Q2=split(Nugg$Qualite$Q2[(Nugg$Qualite$alg=="alg4Iso"|Nugg$Qualite$alg=="Aniso"|Nugg$Qualite$alg=="Iso")&Nugg$Qualite$taille==size],
         repet)
RMSE=split(Nugg$Qualite$RMSE[(Nugg$Qualite$alg=="alg4Iso"|Nugg$Qualite$alg=="Aniso"|Nugg$Qualite$alg=="Iso")&Nugg$Qualite$taille==size],
           repet)

par(mfrow=c(1,2))
par(mar=c(5.1,5.1,4.1,2.1))
plot(c(1,length(Q2)),c(0,1),xaxt="n",col="white",ylab="Q2",cex.lab=2,cex.axis=1.5,xlab=expression(paste(xi,'(%)')))
abline(v=seq(2,(length(Q2)-1),by=3),col="grey")
boxplot(Q2,xaxt="n",add=TRUE,yaxt="n",col=c("#f0f0f0","#bdbdbd","#636363"))
axis(1,seq(2,(length(Q2)-1),by=3),unique(nugg),cex.axis=1.5)
legend("bottomleft",c("IsoGroup","Aniso","Iso"),fill=c("#f0f0f0","#bdbdbd","#636363"),cex=1.5)


par(mar=c(5.1,5.1,4.1,2.1))
plot(c(1,length(Q2)),range(unlist(RMSE)),xaxt="n",col="white",ylab="RMSE",cex.lab=2,cex.axis=1.5,xlab=expression(paste(xi,'(%)')))
abline(v=seq(2,(length(Q2)-1),by=3),col="grey")
boxplot(RMSE,add=TRUE,xaxt="n",yaxt="n",col=c("#f0f0f0","#bdbdbd","#636363"))
axis(1,seq(2,(length(Q2)-1),by=3),unique(nugg),cex.axis=1.5)
legend("topleft",c("IsoGroup","Aniso","Iso"),fill=c("#f0f0f0","#bdbdbd","#636363"),cex=1.5)
